from datetime import datetime
from peewee import *

db = SqliteDatabase('employees.db')


# ________________for peewee________________
class Base(Model):
    """
    Базовый класс для конфигурации БД.
    """
    class Meta:
        database = db


class Employee(Base):
    """
    Модель сотрудника в БД.
    """
    login = CharField(primary_key=True)
    password = CharField()
    current_salary = FloatField()
    next_increase = DateField()


class Token(Base):
    """
    Модель токена в БД.
    """
    value = CharField(unique=True)
    employee = ForeignKeyField(Employee, related_name='token', on_delete='cascade')
    created_at = DateTimeField(default=datetime.now)
