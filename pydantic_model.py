from datetime import datetime
from pydantic import BaseModel, Field


class AddedEmployee(BaseModel):
    """
    Модель для валидации запроса на редактирование и создание записи сотрудника в БД.
    """
    login: str
    password: str
    current_salary: float = Field(ge=0)
    next_increase: datetime
    master_password: str
