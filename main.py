from fastapi import FastAPI
from db_models import *
from secrets import compare_digest, token_urlsafe
from hashlib import sha256
from datetime import timedelta
from pydantic_model import AddedEmployee

Employee.create_table()
Token.create_table()

app = FastAPI(title="Salary APP")

# Мастер-пароль для авторизации при создании и редактировании записи сотрудника в БД (строка).
master_password: str = '12345'

# Срок действия токена (дней).
token_expiration_period: int = 30


@app.post('/add_or_change')
def add_staff(data: AddedEmployee):
    """
    Эндпоинт для создания и редактирования записи сотрудника.
    Доступ предоставляется при корректном master_password.
    Зашифровывает пароль и сохраняет запись в базе.

    :param data: Instance of AddedEmployee.
    :return: Dict.
    """
    if data.master_password == master_password:
        if Employee.get_or_none(Employee.login == data.login):
            employee = Employee.get(Employee.login == data.login)
            employee.password = sha256(data.password.encode()).hexdigest()
            employee.current_salary = data.current_salary
            employee.next_increase = data.next_increase
            return {
                'status': 202,
                'message': 'The data has been changed.'
            }
        else:
            Employee.create(
                login=data.login,
                password=sha256(data.password.encode()).hexdigest(),
                current_salary=float(data.current_salary),
                next_increase=data.next_increase
            )
            return {
                'status': 201,
                'message': 'A new employee has been added.'
            }
    return {
        'status': 403,
        'message': 'The master password is not correct.'
    }


@app.get('/')
def get_token(login: str, password: str):
    """
    Эндпоинт для получения токена.
    При получении корректных логина и пароля возвращает статус, токен и сервисное сообщение.
    Так же производит запись и обновление закодированного токена, а так же даты его создания в БД.

    :param password: Str.
    :param login: Str.
    :return: Dict.
    """
    if Employee.get_or_none(Employee.login == login):
        input_password = sha256(password.encode()).hexdigest()
        curr_employee = Employee.get(Employee.login == login)
        if compare_digest(curr_employee.password, input_password):
            while True:
                random_token = token_urlsafe(5)
                encoded_token = sha256(random_token.encode()).hexdigest()
                if not Token.get_or_none(Token.value == encoded_token):
                    if not Token.get_or_none(Token.employee == curr_employee):
                        Token.create(
                            value=encoded_token,
                            employee=curr_employee
                        )
                        return {
                            'status': 200,
                            'message': 'A new record has been created',
                            'token': random_token
                        }
                    elif Token.get_or_none(Token.employee == curr_employee):
                        Token.get(Token.employee == curr_employee).value = encoded_token
                        return {
                            'status': 200,
                            'message': 'The token has been updated',
                            'token': random_token
                        }
        return {
            'status': 403,
            'message': 'The password is incorrect.'
        }
    return {
        'status': 404,
        'message': 'Not found.'
    }


@app.get('/get_data')
def get_data(input_token: str):
    """
    Эндпоинт для получения данных о текущей зарплате и дате следующего повышения.

    :param input_token: Str.
    :return: Dict.
    """
    encoded_token = sha256(input_token.encode()).hexdigest()
    if Token.get_or_none(Token.value == encoded_token):
        token_obj = Token.get(Token.value == encoded_token)
        created_at = token_obj.created_at
        expiration_date = created_at + timedelta(days=token_expiration_period)
        if expiration_date > datetime.now():
            current_salary = token_obj.employee.current_salary
            next_increase = token_obj.employee.next_increase
            return {
                'status': 200,
                'employee': token_obj.employee.login,
                'current_salary': current_salary,
                'next_increase': next_increase
            }
        return {
            'status': 403,
            'message': 'The token is deprecated'
        }
    return {
        'status': 404,
        'message': 'The token not found '
    }
